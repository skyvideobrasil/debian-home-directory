#install Python 3.7.3 Debian 9

#Start by installing the packages necessary to build Python source:
sudo apt update
sudo apt install build-essential checkinstall zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget
sudo apt-get install libreadline-gplv2-dev libsqlite3-dev tk-dev libc6-dev libbz2-dev

#Download the latest release’s source code from the Python download page using the following curl command:
curl -O https://www.python.org/ftp/python/3.7.3/Python-3.7.3.tgz

#When download is complete, extract the tarball:
tar -xzf Python-3.7.3.tgz

#Navigate to the Python source directory and run the configure script that will perform a number 
#of checks to make sure all of the dependencies on your system are present:
cd Python-3.7.3
sudo ./configure --enable-optimizations

#Run make to start the build process:
sudo make -j 8
#For faster build time, modify the -j flag according to your processor. 
#If you do not know the number of cores your processor you can find it by typing nproc. 
#My system has 8 cores, so I am using the -j8 flag.

#Once the build is done install the Python binaries by running the following command as a user with sudo access:
sudo make altinstall
#Do not use the standard make install as it will overwrite the default system python3 binary.

#At this point, Python 3.7 is installed on your Debian system and ready to be used. You can verify it by typing:
python3.7 --version

#You have installed Python 3.7 on your Debian 9 machine. 
#You can start installing third-party modules with Pip and developing your Python 3 project.

#To install pip in Linux, run the appropriate command for your distribution as follows:
sudo atp-get install python3-pip